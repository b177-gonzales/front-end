import { Fragment, useEffect, useState, useContext } from 'react';
// import productsData from '../data/productsData';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';

export default function Products() {
	// Checks to see if the mock data was captured
	//console.log(productsData);
	//console.log(productsData[0]);

	const { user } = useContext(UserContext);

	// State that will be used to store the products retrieved from the database
	const [products, setProducts] = useState([]);

	// Retrieves the products from the database upon initial render of the "Products" component
	const fetchData = () => {
		fetch('https://infinite-river-50138.herokuapp.com/products/all')
		.then(res => res.json())
		.then(data => {
			console.log(data);

			//Sets the "products" state to map the data retrieved from the fetch request into several "ProductCard" component
			setProducts(data.map(product => {
				
				return (
					<ProductCard key={product._id} productProp={product} />
				);
			}))
			// setProducts(data);
		})
	}

	useEffect(() => {
		fetchData()
	}, []);

	
	return(
		<Fragment>
			{products}
		</Fragment>
	)
}