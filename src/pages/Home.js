import { Fragment } from 'react';
import Banner from '../components/Banner';
// import ProductCard from '../components/ProductCard';

export default function Home(){

	const data = {
	    title: "Awesome Random Shop!",
	    content: "Random quality products",
	    destination: "/products",
	    label: "Shop now!"
	}

	return (
		<Fragment>
			<Banner data={data}/>
		</Fragment>
	)
}
